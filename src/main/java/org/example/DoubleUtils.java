package org.example;

public class DoubleUtils {
    private static final double EPS = 1e-10;

    public static boolean isNotNumber(double value) {
        return Double.isNaN(value) || Double.isInfinite(value);
    }

    public static boolean isEquals(double value1, double value2) {
        return Math.abs(value1 - value2) < EPS;
    }

    public static boolean isZero(double value) {
        return isEquals(value, 0.0);
    }

    private DoubleUtils() {
    }
}
