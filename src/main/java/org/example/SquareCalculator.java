package org.example;

public class SquareCalculator {
    /**
     * Функция решения квадратного уравнения.
     * a,b,c - коэффициенты квадратного уравнения
     * @return список корней квадратного уравнения
     */
    public static double[] solve(double a, double b, double c) {
        if(DoubleUtils.isNotNumber(a) || DoubleUtils.isNotNumber(b) || DoubleUtils.isNotNumber(c)) {
            throw new IllegalArgumentException("All arguments must be numbers!");
        }
        if(DoubleUtils.isZero(a)) {
            throw new IllegalArgumentException("Argument 'a' can not be 0!");
        }
        double d = b*b - 4*a*c;
        if (DoubleUtils.isZero(d)) {
            return new double[]{-b / (2 * a), -b / (2 * a)};
        } else {
            if (d > 0) {
                return new double[]{(-b+Math.sqrt(d))/(2*a), (-b-Math.sqrt(d))/(2*a) };
            } else {
                return new double[0];
            }
        }
    }

    private SquareCalculator() {
    }
}
