package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class SquareCalculatorTest {

    @Test
    void testEmptyResult() {
        //Given
        double a = 1, b = 0, c = 1;
        //When
        double[] result = SquareCalculator.solve(a, b, c);
        //Then
        Assertions.assertEquals(0, result.length);
    }

    @Test
    void testTwoRoots() {
        //Given
        double a = 1, b = 0, c = -1;
        //When
        double[] result = SquareCalculator.solve(a, b, c);
        //Then
        Assertions.assertEquals(2, result.length);
        Assertions.assertTrue(DoubleUtils.isEquals(1, result[0]));
        Assertions.assertTrue(DoubleUtils.isEquals(-1, result[1]));
    }

    @Test
    void testOneRootTwoMultiplicity() {
        //Given
        double a = 1, b = 2, c = 1;
        //When
        double[] result = SquareCalculator.solve(a, b, c);
        //Then
        Assertions.assertEquals(2, result.length);
        Assertions.assertTrue(DoubleUtils.isEquals(-1, result[0]));
        Assertions.assertTrue(DoubleUtils.isEquals(-1, result[1]));
    }

    @Test
    void testOneRootTwoMultiplicityByDCloseToZero() {
        //Given
        double a = 1e-10, b = 2e-11, c = 1e-10;
        //When
        double[] result = SquareCalculator.solve(a, b, c);
        //Then
        Assertions.assertEquals(2, result.length);
        Assertions.assertTrue(DoubleUtils.isEquals(-0.1, result[0]));
        Assertions.assertTrue(DoubleUtils.isEquals(-0.1, result[1]));
    }

    @Test
    void testFirstCoeffCanNotBeZero() {
        //Given
        double a = 0, b = 2, c = 1;
        //When-Then
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> SquareCalculator.solve(a, b, c)
        );
    }

    @Test
    void testFirstCoeffCanNotBeCloseToZero() {
        //Given
        double a = 1e-11, b = 2, c = 1;
        //When-Then
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> SquareCalculator.solve(a, b, c)
        );
    }

    @ParameterizedTest
    @MethodSource("nonNumbersTestCases")
    void testCoeffCanNotBeAnythingButNumbers(double a, double b, double c) {
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> SquareCalculator.solve(a, b, c)
        );
    }

    public static Stream<Arguments> nonNumbersTestCases() {
        return Stream.of(
                Arguments.of(Double.NaN, 2, 1),
                Arguments.of(1, Double.NaN, 1),
                Arguments.of(1, 2, Double.NaN),
                Arguments.of(Double.POSITIVE_INFINITY, 2, 1),
                Arguments.of(1, Double.POSITIVE_INFINITY, 1),
                Arguments.of(1, 2, Double.POSITIVE_INFINITY),
                Arguments.of(Double.NEGATIVE_INFINITY, 2, 1),
                Arguments.of(1, Double.NEGATIVE_INFINITY, 1),
                Arguments.of(1, 2, Double.NEGATIVE_INFINITY)
        );
    }
}
